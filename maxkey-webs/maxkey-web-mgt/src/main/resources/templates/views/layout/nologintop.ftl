<!--top-->
<div id="topBar">
    <div class="container">
        <div class="row">
            <div class="col-sm-7">
                <div style="float:left;;margin-top: 5px;"><IMG SRC="<@base/>/static/images/logo.jpg"
                                                               style="width:55px;heigth:55px"></div>
                <div style="margin-top:15px;margin-left:10px;float:left">
                    <div style="font-size:24px;font-weight:bolder;"><@locale code="global.application"/></div>
                </div>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-4">
                <div style="margin-top:30px;margin-right:10px;float:right;">
                    <table>
                        <tr>
                            <td><@locale code="global.change.language"/> :</td>
                            <td>
                                <div>
                                    <a href="<@currUrl/>?language=en">
                                        <@locale code="global.change.language.en"/>
                                    </a>|
                                    <a href="<@currUrl/>?language=zh_CN">
                                        <@locale code="global.change.language.zh"/>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!--top end-->
